# git-uncrustify

This is a command line utility for code format checking and is designed to be
used as a Git pre-commit hook.

It is built around Uncrustify - "A source code beautifier for C, C++, C#,
ObjectiveC, D, Java, Pawn and VALA".

    https://github.com/uncrustify/uncrustify

## Getting started

* Install uncrustify and add it to your system path.
* Add git-uncrustify to your system path.
* Change to your repository's directory.
* Create an uncrustify configuration file.
* Create a default git-uncrustify configuration file:

    git-uncrustify --init

* Modify .uncrustify.ini to suit your needs.
* Check your repository:

    git-uncrustify.py --checkrepo

* Done

Run 'git-uncrustify.py --help' for a full list of options.

## Copyright and license

This project is licensed under the terms of the MIT license.
