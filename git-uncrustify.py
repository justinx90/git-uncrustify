#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import subprocess
import fnmatch
import argparse
import tempfile

try:
    import configparser
except ImportError:
    import ConfigParser as configparser


# If this configuration file exists it will override the defaults.
_configfile = '.uncrustify.ini'

# Full path of the Uncrustify executable.
_uncrustify_bin = ''

# Full path of the Uncrustify configuration.
_uncrustify_cfg = ''

# List of file extensions to check.
_extensions = []

# List of exclude dirs.
_excludedirs = []

# List of exclude files.
_excludefiles = []


def check_excludefiles(file, excludefiles):
    for exfile in excludefiles:
        filepath = os.path.abspath(file).decode()
        exfilepath = os.path.abspath(exfile)
        if filepath == exfilepath:
            return True

    return False


def check_excludedirs(file, excludedirs):
    for exdir in excludedirs:
        filepath = os.path.abspath(file)
        exdirpath = os.path.abspath(exdir).encode()
        if filepath.startswith(exdirpath):
            return True

    return False


def check_includeext(file, extensions):
    for ext in extensions:
        if fnmatch.fnmatchcase(file, ext.encode()):
            return True

    return False


def format_file(file):
    cmd = [_uncrustify_bin, '-c', _uncrustify_cfg, '--replace', '--no-backup', file]
    child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child.wait()

    return child.returncode


def check_format(file):
    cmd = [_uncrustify_bin, '--check', '-c', _uncrustify_cfg, file]
    child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child.wait()

    return child.returncode


def check_format_staged(file):
    rc = 0

    try:
        filename, fileExtension = os.path.splitext(file)

        # Create a temp file.
        temp = tempfile.NamedTemporaryFile(suffix=fileExtension, delete=False)
        temp.close()

        # Output the file as it is staged to a temp file.
        cmd = 'git show :"%(file)s" > %(temp)s' % {"file": file, "temp": temp.name}
        os.system(cmd)

        # Checkout formatting of temp file.
        rc = check_format(temp.name)

    finally:
        temp.close()
        os.unlink(temp.name)

    return rc


def check_config(config):
    rc = 0

    temp = tempfile.NamedTemporaryFile(delete=False)
    temp.close()

    cmd = [_uncrustify_bin, '--update-config', '-c', config, '-o', temp.name]
    child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child.wait()
    results = child.communicate()

    for result in results:
        if (result.lower().find('unknown'.encode()) != -1) or (result.lower().find('wrong'.encode()) != -1):
            print(result.decode())
            rc = -1
            break

    os.unlink(temp.name)

    return rc


def update_from_config(config):
    global _uncrustify_bin
    global _uncrustify_cfg
    global _extensions
    global _excludedirs
    global _excludefiles

    if config.get('config', 'uncrustify'):
        _uncrustify_bin = config.get('config', 'uncrustify')

    if config.get('config', 'config'):
        _uncrustify_cfg = os.path.join(os.path.dirname(os.path.realpath(_configfile)), config.get('config', 'config'))

    if config.get('config', 'extensions'):
        _extensions = config.get('config', 'extensions').replace(" ", "").split(',')

    if config.get('config', 'excludedirs'):
        _excludedirs = config.get('config', 'excludedirs').replace(", ", ",").split(',')

    if config.get('config', 'excludefiles'):
        _excludefiles = config.get('config', 'excludefiles').replace(", ", ",").split(',')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--config',                                             help='Configuration file.')
    parser.add_argument('--uncrustify-cfg',                                     help='Uncrustify configuration. Will override value in configuration file.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--init', action='store_true',                           help='Create initial configuration.')
    group.add_argument('--install', action='store_true',                        help='Install as pre-commit hook.')
    group.add_argument('-f', '--file', nargs='+',                               help='Apply formatting to file(s).')
    group.add_argument('-d', '--dir', nargs='+', default=False,                 help='Apply formatting to a directory(s) of files.')
    group.add_argument('-m', '--modified', action='store_true', default=False,  help='Apply formatting to modified files only.')
    group.add_argument('-s', '--staged', action='store_true', default=False,    help='Apply formatting to staged files only.')
    group.add_argument('-cf', '--checkfile', nargs='+', default=False,          help='Perform file(s) check.')
    group.add_argument('-pc', '--precommit', action='store_true', default=False, help='Perform pre-commit check.')
    group.add_argument('-cr', '--checkrepo', action='store_true', default=False, help='Perform conformance check.')
    args = parser.parse_args()

    # Check for user configuration file.
    if args.config:
        _configfile = os.path.abspath(args.config)
    else:
        _configfile = os.path.join(os.getcwd(), _configfile)

    if args.init:
        print('Creating configuration.')

        config = configparser.ConfigParser(allow_no_value=True)

        config.add_section('config')
        config.set('config', '; Uncrustify executable filename.')
        config.set('config', 'uncrustify', 'uncrustify')
        config.set('config', '; Uncrustify configuration filename.')
        config.set('config', 'config', '.uncrustify.cfg')
        config.set('config', '; List of file extensions to check.')
        config.set('config', 'extensions', '*.c, *.h, *.cpp, *.hpp')
        config.set('config', '; List of directories (relative to this file) to exclude.')
        config.set('config', 'excludedirs', '')
        config.set('config', '; List of files (relative to this file) to exclude.')
        config.set('config', 'excludefiles', '')

        with open(_configfile, 'w') as configfile:
            config.write(configfile)

        config.read(_configfile)
        update_from_config(config)
        sys.exit(0)

    if (os.path.isfile(_configfile)):
        config = configparser.ConfigParser()
        config.read(_configfile)
        update_from_config(config)

    else:
        pass

    # Override the configured option.
    if args.uncrustify_cfg:
        _uncrustify_cfg = args.uncrustify_cfg

    # Normalise configured paths.
    _uncrustify_bin = os.path.normpath(_uncrustify_bin)
    _uncrustify_cfg = os.path.normpath(_uncrustify_cfg)
    _excludedirs[:] = [os.path.normpath(os.path.join(os.path.dirname(_configfile),dir)) for dir in _excludedirs]
    _excludefiles[:] = [os.path.normpath(os.path.join(os.path.dirname(_configfile),dir)) for dir in _excludefiles]

    if not args.precommit:
        print('config         : %(configfile)s' % {'configfile': _configfile})
        print('uncrustify bin : %(uncrustify_bin)s' % {'uncrustify_bin': _uncrustify_bin})
        print('uncrustify cfg : %(uncrustify_cfg)s' % {'uncrustify_cfg': _uncrustify_cfg})
        print('extensions     : %(extensions)s' % {'extensions': _extensions})
        print('exclude dirs   : %(excludedirs)s' % {'excludedirs': _excludedirs})
        print('exclude files  : %(excludefiles)s\n' % {'excludefiles': _excludefiles})


    # Check uncrustify exists.
    try:
        cmd = [_uncrustify_bin, '-v']
        child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child.wait()

    except:
        print('Error: Missing Uncrustify')
        sys.exit(1)

    # Check uncrustify configuration exists.
    if not (os.path.isfile(_uncrustify_cfg)):
        print('Error: Missing Uncrustify configuration.')
        sys.exit(1)

    # Check uncurstify configuration is valid.
    if check_config(_uncrustify_cfg):
        print('Error: Invalid Uncrustify configuration.')
        sys.exit(1)

    if args.install:
        print('Installing pre-commit hook.')
        filename = os.path.join(os.path.dirname(os.path.realpath(_configfile)), '.git/hooks/pre-commit')
        if (not os.path.isfile(filename)):
            if not os.path.exists(os.path.dirname(filename)):
                try:
                    os.makedirs(os.path.dirname(filename))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

            with open(filename, 'w') as output_file:
                pre_commit_hook = '#!/bin/bash\n"%(python)s" "%(gituncrustify)s" --precommit' % {'python': sys.executable, 'gituncrustify': os.path.abspath(sys.argv[0])}
                output_file.write(pre_commit_hook)
                print('  Pre-commit hook installed to %(filename)s' % {'filename': filename})
                sys.exit(0)
        else:
            print('  Pre-commit hook already installed at %(filename)s. Remove and try again.' % {'filename': filename})
            sys.exit(1)

    if args.file:
        print('Formatting selected files.')
        for file in args.file:
            if not check_excludedirs(file, _excludedirs):
                if not check_excludefiles(file, _excludefiles):
                    if check_includeext(file, _extensions):            
                        format_file(os.path.abspath(file))
                        print('  %(file)s' % {'file': os.path.abspath(file)})

    if args.dir:
        print('Formatting selected directory of files.')
        for rootdir in args.dir:
            for subdir, dirs, files in os.walk(rootdir):
                for file in files:
                    if not check_excludedirs(os.path.abspath(os.path.join(subdir, file)).encode(), _excludedirs):
                        if not check_excludefiles(os.path.abspath(os.path.join(subdir, file)).encode(), _excludefiles):
                            if check_includeext(file.encode(), _extensions):
                                file2 = os.path.join(subdir, file)
                                format_file(os.path.abspath(file2))
                                print('  %(file)s' % {'file': os.path.abspath(file2)})

    if args.modified:
        print('Formatting modified files.')

        # Create a list of all modified files.
        cmd = ['git', 'status', '--porcelain']
        output = subprocess.check_output(cmd)
        modifiedfiles = output.splitlines()
        modifiedfiles = list(filter(None, modifiedfiles))

        cmd = ['git', 'rev-parse', '--show-toplevel']
        reporoot = subprocess.check_output(cmd).rstrip()

        # If any modified file has changes then abort.
        temp = []
        for modifiedfile in modifiedfiles:
            if not check_excludedirs(modifiedfile, _excludedirs):
                if not check_excludefiles(modifiedfile, _excludefiles):
                    if check_includeext(modifiedfile, _extensions):
                        if modifiedfile.find(' M'.encode(), 0, 2) != -1:
                            file = os.path.join(reporoot, modifiedfile[3:])
                            format_file(os.path.abspath(file).decode())
                            print('  %(file)s' % {'file': os.path.abspath(file).decode()})

    if args.staged:
        print('Formatting staged files.')

        # Create a list of staged files only.
        cmd = ['git', 'diff', '--relative', '--cached', '--name-only']
        output = subprocess.check_output(cmd)
        stagedfiles = output.splitlines()
        stagedfiles = list(filter(None, stagedfiles))
        if len(stagedfiles) == 0:
            print('No staged files.')
            sys.exit(1)

        # Create a list of all tracked files.
        cmd = ['git', 'status', '--porcelain']
        output = subprocess.check_output(cmd)
        trackedfiles = output.splitlines()
        trackedfiles = list(filter(None, trackedfiles))

        # If any staged file has unstaged changes then abort.
        temp = []
        for trackedfile in trackedfiles:
            for stagedfile in stagedfiles:
                if not check_excludedirs(stagedfile, _excludedirs):
                    if not check_excludefiles(stagedfile, _excludefiles):
                        if check_includeext(stagedfile, _extensions):
                            if trackedfile.find(stagedfile) != -1:
                                if trackedfile.find('MM'.encode(), 0, 2) != -1:
                                    temp.append(stagedfile)

        if len(temp) > 0:
            print('The following staged files have unstaged changes.')
            for file in temp:
                print('  %(file)s' % {'file': file.decode()})
            print('\nAdd the unstaged changes or un-stage the offending file(s) and try again.')
            sys.exit(1)

        for stagedfile in stagedfiles:
            if not check_excludedirs(stagedfile, _excludedirs):
                if not check_excludefiles(stagedfile, _excludefiles):
                    if check_includeext(stagedfile, _extensions):
                        print('  %(file)s' % {'file': os.path.abspath(stagedfile.decode())})
                        cmd = [_uncrustify_bin, '-c', _uncrustify_cfg, '--replace', '--no-backup', os.path.abspath(stagedfile.decode())]
                        child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        child.wait()

        print('\nStaged files have been formatted.')
        print('  (Check and then add the unstaged changes "git add <filename>")')

        sys.exit(0)

    if args.checkfile:
        print('Checking formatting of file(s).')
        failed = 0
        for file in args.checkfile:
            if check_format(os.path.abspath(file)):
                print('  Fail. %(file)s' % {"file": file})
                failed += 1

        if failed:
            print('\n  (use "git-uncrustify.py --file <filename>" to format the offending file.)')

        sys.exit(failed)


    if args.precommit:
        print('Checking formatting of staged files.')

        # Create a list of staged files.
        cmd = ['git', 'diff', '--cached', '--name-only']
        output = subprocess.check_output(cmd)
        files = output.splitlines()
        files = list(filter(None, files))

        # Check formatting of staged files.
        failed = 0
        for file in files:
            if not check_excludedirs(file, _excludedirs):
                if not check_excludefiles(file, _excludefiles):
                    if check_includeext(file, _extensions):
                        if check_format_staged(file.decode()):
                            print('  Fail. %(file)s' % {"file": file.decode()})
                            failed += 1

        if failed:
            print('\n  (use "git-uncrustify.py --staged" to format the offending files.)')

        sys.exit(failed)

    if args.checkrepo:
        print('Checking repo.')

        # Create a list of file in the repo.
        cmd = ['git', 'rev-parse', '--abbrev-ref', 'HEAD']
        branch = subprocess.check_output(cmd).decode().rstrip()
        print('Branch %(branch)s' % {'branch': branch})

        cmd = ['git', 'ls-tree', '--name-only', '-r', branch]
        output = subprocess.check_output(cmd)
        trackedfiles = output.splitlines()
        trackedfiles = list(filter(None, trackedfiles))

        # Check formatting files.
        failed = 0
        for file in trackedfiles:
            if not check_excludedirs(file, _excludedirs):
                if not check_excludefiles(file, _excludefiles):
                    if check_includeext(file, _extensions):
                        if check_format(file.decode()):
                            print('  Fail. %(file)s' % {"file": os.path.abspath(file.decode())})
                            failed = 1
                            break

        if failed:
            print('\n  (use "git-uncrustify.py --file <filename>" to format the offending file.)')

        sys.exit(failed)
