#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess
import unittest

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

_untracked_file_clean = '\
void main(void)\n\
{\n\
    for (;;)\n\
    {\n\
        if (false)\n\
        {\n\
        }\n\
    }\n\
}\n'

_untracked_file_dirty = '\
void main(void) {\n\
    for (;;) {\n\
        if (false)\n\
        {\n\
        }\n\
    }\n\
}\n'


def execute(cmd):
    child = subprocess.Popen(cmd);
    child.wait()
    return child.returncode


def re_create_file(file, contents):
    if os.path.exists(file):
        os.remove(file)
    with open(file, 'w') as output_file:
        output_file.write(contents)
    output_file.close()


def git_discard_all_changes():
    cmd = ['git', 'checkout', '../test']
    execute(cmd)


def clean_untracked_files():
    test_file = 'test_untracked_file.c'
    if os.path.exists(test_file):
        os.remove(test_file)


def add_pre_commit_hook():
    pre_commit = '../.git/hooks/pre-commit'
    with open(pre_commit, 'w') as output_file:
        output_file.write('pre_commit_hook')
    output_file.close()

def remove_pre_commit_hook():
    pre_commit = '../.git/hooks/pre-commit'
    if os.path.exists(pre_commit):
        os.remove(pre_commit)


def make_file_dirty(file):
    cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--config', '.uncrustify_dirty.ini', '--file', file]
    execute(cmd)


def make_file_clean(file):
    cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--file', file]
    execute(cmd)


class TestInstall(unittest.TestCase):
    def setUp(self):
        remove_pre_commit_hook()

    def tearDown(self):
        remove_pre_commit_hook()

    def test_install_clean(self):
        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--install']
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)

    def test_install_dirty(self):
        add_pre_commit_hook()

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--install']
        returncode = execute(cmd)

        self.assertTrue(returncode != 0)


class TestFile(unittest.TestCase):
    def setUp(self):
        git_discard_all_changes()

    def tearDown(self):
        pass

    def test_format_clean_file_subdir(self):
        file = 'single_file_dir/file0.c'

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--file', file]
        execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file]
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)

    def test_format_dirty_file_subdir(self):
        file = 'single_file_dir/file0.c'
        make_file_dirty(file)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--file', file]
        execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file]
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)

    def test_format_dirty_files_subdir(self):
        file1 = 'multi_file_dir/file0.c'
        file2 = 'multi_file_dir/file1.c'
        make_file_dirty(file1)
        make_file_dirty(file2)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--file', file1, file2]
        execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file1, file2]
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)


class TestDir(unittest.TestCase):
    def setUp(self):
        git_discard_all_changes()

    def tearDown(self):
        pass

    def test_format_dirty_dir(self):
        file1 = 'multi_file_dir/file0.c'
        file2 = 'multi_file_dir/file1.c'
        file3 = 'single_file_dir/file0.c'
        make_file_dirty(file1)
        make_file_dirty(file2)
        make_file_dirty(file3)
        dir1 = 'single_file_dir'
        dir2 = 'multi_file_dir'

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--dir', dir1, dir2]
        execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkrepo']
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)


class TestModified(unittest.TestCase):
    def setUp(self):
        git_discard_all_changes()
        re_create_file('test_untracked_file.c', _untracked_file_clean)

    def tearDown(self):
        clean_untracked_files()
        git_discard_all_changes()

    def test_modified_file(self):
        file1 = 'test_untracked_file.c'
        file2 = 'single_file_dir/file0.c'
        make_file_dirty(file1)
        make_file_dirty(file2)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--modified']
        execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file1]
        returncode1 = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file2]
        returncode2 = execute(cmd)

        self.assertTrue(((returncode1 == 0) == (returncode2 != 0)))


class TestStaged(unittest.TestCase):
    def setUp(self):
        git_discard_all_changes()
        re_create_file('test_untracked_file.c', _untracked_file_clean)

    def tearDown(self):
        git_discard_all_changes()
        clean_untracked_files()

    def test_staged_clean_file_pre_commit(self):
        cmd = ['git', 'add', 'test_untracked_file.c']
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--precommit']
        returncode = execute(cmd)

        # A staged clean file should pass the pre-commit test.
        self.assertTrue(returncode == 0)

    def test_staged_dirty_file_pre_commit(self):
        make_file_dirty('test_untracked_file.c')

        cmd = ['git', 'add', 'test_untracked_file.c']
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--precommit']
        returncode = execute(cmd)

        # A staged dirty file should fail the pre-commit test.
        self.assertTrue(returncode != 0)

    def test_staged_clean_file_format(self):
        cmd = ['git', 'add', 'test_untracked_file.c']
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--staged']
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', 'test_untracked_file.c']
        returncode = execute(cmd)

        # A staged clean file should pass a format test after stage formatting.
        self.assertTrue(returncode == 0)

    def test_staged_dirty_file_format(self):
        make_file_dirty('test_untracked_file.c')

        cmd = ['git', 'add', 'test_untracked_file.c']
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--staged']
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', 'test_untracked_file.c']
        returncode = execute(cmd)

        # A staged dirty file should pass a format test after stage formatting.
        self.assertTrue(returncode == 0)


    def test_staged_excluded_dirty_file_format(self):
        file1 = 'exclude_ext/single_file_dir/file0.cpp'
        file2 = 'exclude_dir/multi_file_dir/file0.c'
        make_file_dirty(file1)
        make_file_dirty(file2)

        cmd = ['git', 'add', file1]
        returncode = execute(cmd)

        cmd = ['git', 'add', file2]
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--staged']
        returncode = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file1]
        returncode1 = execute(cmd)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file2]
        returncode2 = execute(cmd)

        # A staged dirty file that is excluded should fail a format test after stage formatting.
        self.assertTrue(returncode1 != 0)
        self.assertTrue(returncode2 != 0)


class TestCheckfile(unittest.TestCase):
    def setUp(self):
        git_discard_all_changes()

    def tearDown(self):
        git_discard_all_changes()

    def test_check_clean_file(self):
        file = 'single_file_dir/file0.c'

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file]
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)

    def test_check_dirty_file(self):
        file = 'single_file_dir/file0.c'
        make_file_dirty(file)

        cmd = ['python', '../git-uncrustify.py', '--config', '../.uncrustify.ini', '--checkfile', file]
        returncode = execute(cmd)

        self.assertTrue(returncode != 0)

class TestInit(unittest.TestCase):
    def setUp(self):
        git_discard_all_changes()

    def tearDown(self):
        git_discard_all_changes()

    def test_init_default(self):
        file = '.uncrustify.ini'

        cmd = ['python', '../git-uncrustify.py', '--init']
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)
        self.assertTrue(os.path.exists(file))

    def test_init_user(self):
        file = 'test.ini'

        cmd = ['python', '../git-uncrustify.py', '--init', '--config', file]
        returncode = execute(cmd)

        self.assertTrue(returncode == 0)
        self.assertTrue(os.path.exists(file))


if __name__ == '__main__':
    unittest.main()

    cmd = ['git', 'checkout', 'test_untracked_file.c', '&', 'git', 'reset', 'test_untracked_file.c', '&', 'rm', 'test_untracked_file.c']
    execute(cmd)
